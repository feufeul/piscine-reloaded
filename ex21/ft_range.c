/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/03 14:48:45 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/03 14:57:07 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_range(int min, int max)
{
	int		*tab_int;
	int		size;
	int		cpt;

	size = max - min;
	tab_int = NULL;
	if (size > 0)
	{
		tab_int = malloc(sizeof(int) * (size));
		if (tab_int)
		{
			cpt = 0;
			while (cpt < size)
			{
				tab_int[cpt] = min++;
				cpt++;
			}
		}
	}
	return (tab_int);
}
