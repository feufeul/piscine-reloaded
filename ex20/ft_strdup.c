/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/03 14:48:07 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/03 15:32:37 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strlen(char *src)
{
	int	size;

	size = 0;
	while (*src)
	{
		size++;
		src++;
	}
	return (size);
}

char	*ft_strdup(char *src)
{
	int		size;
	char	*final_str;

	size = ft_strlen(src);
	final_str = malloc(sizeof(char) * size + 1);
	if (final_str)
	{
		final_str[size] = '\0';
		while (size > 0)
		{
			final_str[size - 1] = src[size - 1];
			size--;
		}
	}
	return (final_str);
}
