/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_file.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/03 14:49:28 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/03 16:22:39 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <unistd.h>
#include "display_file.h"
#define BUFFER_SIZE 4096

void	ft_display_file(char *src)
{
	int		fd;
	char	buffer[BUFFER_SIZE + 1];
	int		ret;

	fd = open(src, O_RDONLY);
	if (fd == -1)
	{
		ft_puterr("open() error.\n");
		return ;
	}
	ret = read(fd, buffer, BUFFER_SIZE);
	buffer[ret] = '\0';
	ft_putstr(buffer);
	if (close(fd) == -1)
		ft_puterr("close() error.\n");
}

int		main(int argc, char *argv[])
{
	if (argc == 1)
		ft_puterr("File name missing.\n");
	else if (argc > 2)
		ft_puterr("Too many arguments.\n");
	else if (argc == 2)
		ft_display_file(argv[1]);
	return (0);
}
