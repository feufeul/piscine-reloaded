/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/03 14:41:28 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/03 15:29:48 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_sort_tab(char **argv);
void	ft_print_params(char **argv);
void	ft_putstr(char *str);
void	ft_putchar(char c);
int		ft_strcmp(char *str1, char *str2);

int		main(int argc, char *argv[])
{
	if (argc > 2)
		ft_sort_tab(argv);
	if (argc > 1)
		ft_print_params(argv);
	return (0);
}

void	ft_putstr(char *str)
{
	while (*str)
		ft_putchar(*str++);
}

void	ft_print_params(char **argv)
{
	argv++;
	while (*argv)
	{
		ft_putstr(*argv++);
		ft_putchar('\n');
	}
}

int		ft_strcmp(char *str1, char *str2)
{
	while (*str1 && *str2 && *str1 == *str2)
	{
		str1++;
		str2++;
	}
	return (*str1 - *str2);
}

void	ft_sort_tab(char **argv)
{
	int		i;
	char	*tmp;

	i = 2;
	while (argv[i])
	{
		if (ft_strcmp(argv[i], argv[i - 1]) < 0)
		{
			tmp = argv[i];
			argv[i] = argv[i - 1];
			argv[i - 1] = tmp;
			i = 1;
		}
		i++;
	}
}
